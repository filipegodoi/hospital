package Cadastro;

public class Consulta {

	private String nomeConsulta;
	private int horarioConsulta;
	private int numeroSala;
	private int numeroRetorno;
	private Paciente paciente;
	private Medico medico;
	
	public String getNomeConsulta() {
		return nomeConsulta;
	}
	
	public void setNomeConsulta(String nomeConsulta){
		this.nomeConsulta = nomeConsulta;
	}
	
	public int getHorarioConsulta() {
		return horarioConsulta;
	}
	
	public void setHorarioConsulta(int horarioConsulta){
		this.horarioConsulta = horarioConsulta;
	}
	
	public int getNumeroSala() {
		return numeroSala;
	}
	
	public void setNumeroSala(int numeroSala) {
		this.numeroSala = numeroSala;
	}
	
	public int getnumeroRetorno() {
		return numeroRetorno;
	}
	
	public void setnumeroRetorno(int numeroRetorno) {
		this.numeroRetorno = numeroRetorno;
	}
	
	public Paciente getPaciente() {
		return paciente;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	
}
