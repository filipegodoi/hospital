package Cadastro;
import java.util.ArrayList;

public class ControleConsulta {

public ArrayList<Consulta> listaConsulta;
	
	public void ControlaConsulta(){
		listaConsulta = new ArrayList<Consulta>();
	}
	
	public void adicionar(Consulta consulta){
		listaConsulta.add(consulta);
		System.out.println("Consulta adicionado.");
	}
	
	public void remover(Consulta consulta){
		listaConsulta.remove(consulta);
		System.out.println("Consulta removido.");
	}
	
	public Consulta pesquisarConsulta(String tipoConsulta){
		for(Consulta consulta : listaConsulta){
			if(consulta.getNomeConsulta().equalsIgnoreCase(tipoConsulta)){
				return consulta;
			}
		}
		return null;
	}
	
	public void ConsultasNaLista(){
		for(Consulta consulta : listaConsulta){
			System.out.println(consulta.getnumeroRetorno());
		}
	}
	
	
}
