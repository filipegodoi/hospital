package Cadastro;
import java.util.ArrayList;

public class ControleUsuario {

	public ArrayList<Usuario> listaUsuario;
	
	public void ControlaUsuario(){
		listaUsuario = new ArrayList<Usuario>();
	}
	
	public void adicionar(Usuario usuario){
		listaUsuario.add(usuario);
		System.out.println("Usuario adicionado.");
	}
	
	public void remover(Usuario usuario){
		listaUsuario.remove(usuario);
		System.out.println("Usuario removido.");
	}
	
	public Usuario pesquisarUsuario(String nomeUsuario){
		for(Usuario usuario : listaUsuario){
			if(usuario.getNome().equalsIgnoreCase(nomeUsuario)){
				return usuario;
			}
		}
		return null;
	}
	
	public void UsuariosNaLista(){
		for(Usuario usuario : listaUsuario){
			System.out.println(usuario.getNome());
		}
	}
}
